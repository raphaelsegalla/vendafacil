package br.com.vendafacil.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VendafacilRestApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(VendafacilRestApiApplication.class, args);
	}

}
